#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def a(i:int):
    if i < 0:
        return 'negative'
    elif i == 0:
        return 'ZERO'
    else:
        return 'positive'
