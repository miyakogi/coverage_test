#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from unittest import TestCase

from sample import a


class TestSample(TestCase):
    def test(self):
        self.assertEqual(a(0), 'ZERO')
